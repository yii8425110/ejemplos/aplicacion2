<?php

use yii\helpers\Html;


// realizarlo con foreach
echo "<ul class='list-group list-group-horizontal'>";
foreach ($imagenes as $foto) {
    echo "<li class='list-group-item col-lg-3'>";
    echo Html::img("@web/imgs/{$foto}", [
        'class' => 'img-thumbnail',
    ]);
    echo "</li>";
}
echo "</ul>";
